const Game = require('./Game')
const Analytics = require('./Analytics')

if (process.argv[2] === '-i') {
  const a = new Analytics()
  console.clear()
  a.printScores()
  process.exit()
}

let b = new Game()

b.printGame()
b.inputCell()
