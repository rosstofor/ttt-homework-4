const Cell = require('../game/Cell')
const { BOARDSIZE } = require('../constants/index')

class Board {
  constructor() {
    this.board = []
    for (let i = 0; i < BOARDSIZE; i++) {
      this.board[i] = new Cell(i + 1)
    }
  }
  printBoard() {
    console.log(this.board[0].cellNum, this.board[1].cellNum, this.board[2].cellNum)
    console.log(this.board[3].cellNum, this.board[4].cellNum, this.board[5].cellNum)
    console.log(this.board[6].cellNum, this.board[7].cellNum, this.board[8].cellNum)
  }
}

module.exports = Board
