const readLine = require('readline')
const Board = require('./Board')
const { X, O } = require('../constants/index')
const Analytics = require('./Analytics')

class Game {
  constructor() {
    this.board = new Board()
    this.moveFlag = true
    this.rl = readLine.createInterface({
      input: process.stdin,
      output: process.stdout,
    })
  }

  printGame() {
    console.clear()
    this.moveFlag ? console.log(`${X} Move: `) : console.log(`${O} Move: `)
    this.board.printBoard()
  }

  move(num) {
    if (this.moveFlag) {
      if (parseInt(this.board.board[num - 1].cellNum)) {
        this.changeBoard(X, num)
      } else {
        console.clear()
        this.printGame()
        console.log(`Cell ${num} not empty. Move player ${X}`)
        this.inputCell()
      }
    } else {
      if (parseInt(this.board.board[num - 1].cellNum)) {
        this.changeBoard(O, num)
      } else {
        console.clear()
        this.printGame()
        console.log(`Cell ${num} not empty. Move player ${O}`)
        this.inputCell()
      }
    }
  }

  inputCell() {
    this.rl.question('Input number of cell ---> ', (data) => {
      if (!parseInt(data) || data > 9 || data < 1 || data.length !== 1) {
        console.clear()
        this.printGame()
        console.log('Number must bу between 1 and 9')
        this.inputCell()
      } else {
        this.move(data)
      }
    })
  }

  inputUsername() {
    this.rl.question('Input your username for statistics if you want ---> ', (data) => {
      if (data) {
        const analytics = new Analytics()
        analytics.addScore(data)
        console.log(`${data}, you added to statistics`)
      } else {
        console.log('You are not added to statistics')
      }
      this.rl.close()
    })
  }

  changeBoard(player, num) {
    this.board.board[num - 1].cellNum = player
    this.board.board[num - 1].isFree = false
    if (!this.checkFreeCell()) {
      this.printGame()
      console.log('No winner')
      process.exit()
    }
    if (this.winning(player)) {
      this.printGame()
      console.log(`Win ${player}`)
      this.inputUsername()
    }
    this.moveFlag = !this.moveFlag
    this.printGame()
    this.inputCell()
  }

  checkFreeCell() {
    let noFreeCells = false
    this.board.board.forEach((el) => {
      if (el.isFree) {
        noFreeCells = true
      }
    })
    return noFreeCells
  }

  winning(player) {
    if (
      (this.board.board[0].cellNum === player && this.board.board[1].cellNum === player && this.board.board[2].cellNum === player) ||
      (this.board.board[3].cellNum === player && this.board.board[4].cellNum === player && this.board.board[5].cellNum === player) ||
      (this.board.board[6].cellNum === player && this.board.board[7].cellNum === player && this.board.board[8].cellNum === player) ||
      (this.board.board[0].cellNum === player && this.board.board[3].cellNum === player && this.board.board[6].cellNum === player) ||
      (this.board.board[1].cellNum === player && this.board.board[4].cellNum === player && this.board.board[7].cellNum === player) ||
      (this.board.board[2].cellNum === player && this.board.board[5].cellNum === player && this.board.board[8].cellNum === player) ||
      (this.board.board[0].cellNum === player && this.board.board[4].cellNum === player && this.board.board[8].cellNum === player) ||
      (this.board.board[2].cellNum === player && this.board.board[4].cellNum === player && this.board.board[6].cellNum === player)
    ) {
      return true
    }
    return false
  }
}

module.exports = Game
